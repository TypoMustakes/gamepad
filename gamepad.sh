#!/bin/bash

### gitbot
### Copyright (C) 2021  Miskolczi Richárd
### 
### This program is free software; you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation; either version 2 of the License, or
### (at your option) any later version.
### 
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
### 
### You should have received a copy of the GNU General Public License along
### with this program; if not, write to the Free Software Foundation, Inc.,
### 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###
### Full license: /usr/share/licenses/gamepad/LICENSE
###
### Contact: <miskolczi.richard@protonmail.com>

help()
{
	/usr/bin/echo "Usage: gamepad [option] <mode>
	
OPTIONS:
	- on/off: starts/stops xow driver
	- status: shows whether current state is on or off
	- mode: use this flag as is to query current operation mode, or follow it up with one of the operation modes listed below to switch it
OPERATION MODES:
	- normal: normal mode, duh
	- compat: compatibility mode, will connect as Xbox 360 gamepad
	
ENVIRONMENT VARIABLES:
	- GAMEPAD_NOTIFY:
		values: 'true'/'false'
		default value: 'true'
		If set to 'true', gamepad will send a notification of changes after execution.
		For example, \"Gamepad set to Compatibility mode\"."
}

notify() # so that I wouldn't have to check for $GAMEPAD_NOTIFYs value every time
{
	if [ $GAMEPAD_NOTIFY == 'true' ]
	then
		/usr/bin/notify-send "$1" # sends desktop notification containing passed parameter
	fi
}

if [ -z "$GAMEPAD_NOTIFY" ]
then
	GAMEPAD_NOTIFY='true' # default value
fi

if [ -z "$1" ]
then
	help
elif [ $1 == "on" ]
then
	/usr/bin/sudo /usr/bin/systemctl start xow.service
	notify "Gamepad On"
elif [ $1 == "off" ]
then
	/usr/bin/sudo /usr/bin/systemctl stop xow.service
	notify "Gamepad Off"
elif [ $1 == "mode" ]
then
	restart=false

	if [ -z "$2" ]
	then
		if [ "$(/usr/bin/cat /etc/systemd/system/xow.service.d/override.conf)" == "" ]
		then
			echo "normal"
		else
			echo "compat"
		fi
	else
		if [ "$(/usr/bin/pgrep xow)" != "" ]
		then
			/usr/bin/sudo /usr/bin/systemctl stop xow.service
			restart=true
		fi

		if [ $2 == "normal" ]
		then
			/usr/bin/cat /dev/null > /etc/systemd/system/xow.service.d/override.conf
			notify "Gamepad set to Normal Mode"
		elif [ $2 == "compat" ]
		then
			/usr/bin/echo "\
[Service]
Environment=\"XOW_COMPATIBILITY=1\"" > /etc/systemd/system/xow.service.d/override.conf
			notify "Gamepad set to Compatibility Mode"
		fi

		/usr/bin/sudo /usr/bin/systemctl daemon-reload
	
		if [ "$restart" == true ]
		then
			/usr/bin/sudo /usr/bin/systemctl start xow.service
		fi
	fi
elif [ $1 == "status" ]
then
	if [ "$(/usr/bin/pgrep xow)" != "" ]
	then
		/usr/bin/echo "on"
	else
		/usr/bin/echo "off"
	fi
else
	help
fi
