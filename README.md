## What is this anyway?

You might have heard of the userspace driver for Xbox One gamepads called [`xow`](https://github.com/medusalix/xow). This is a CLI frontend for that. I first made this script because due to a [bug](https://github.com/medusalix/xow/issues/141) related to `libusb-1.0.24`, which caused extremely high CPU usage while `xow` was running. According to comments and commits in the `libusb` GitHub repository, the issue has been resolved, but it still has yet to be merged and shipped as of writing this.

Before resorting to just downgrade my `libusb` version to 1.0.23, I made this script to make it real easy to just toggle `xow` on and off with a keybinding. That's not all tough, as `xow` has a 'compatibility' mode, which will make it so that controllers connected to your dongle will apear as Xbox 360 controllers. This project also makes use of this and allows you to toggle between 'compatibility' and 'normal' modes with ease.

Author of the `xow` project `medusalix` states on the `xow` project's GitHub page, that they decided to abandon the project and instead join in on the development of [`xpadneo`](https://github.com/atar-axis/xpadneo), which, contrary to the userspace nature of `xow` is a kernel driver and has more features than `xow`. It's a really cool project, by all means, but it simply didn't work with my wireless Xbox Series controller adapter when I tried, so I will still attend to this project, seeing that `xow` does indeed still has its use and cannot yet be replaced just like that with `xpadneo`.

## How to install

### Prerequisites

> - `pacman`
>
> - either `git` or any archiving program
>
> - root privileges

### Download

> ### with git:
> 
> > `git clone https://gitlab.com/TypoMustakes/gamepad.git`
> 
> ### without git
> 
> > - visit `https://gitlab.com/TypoMustakes/gamepad/`
> >
> > - click the  (Download) button and choose an archive format
> >
> > - extract the archive

### Installation

> Open the project directory by typing `cd gamepad`
> 
> ### installing existing package
> 
> > `sudo pacman -U gamepad*.pkg.tar.zst`
> 
> ### building your own package
>
> > `makepkg -si`
>
> 
> or add [my repository](https://gitlab.com/TypoMustakes/typomustakes) and download it like you would any other package

## Configuring your /etc/sudoers (optional)

Seeing that I do not yet have a solution to this problem (almost resolved), you have to modify the `/etc/sudoers` file to allow for passwordless execution. The program works fine with a password as well, but if for example you wish to set keybindings and shortcuts to this program then that can be a headache. This action requires root privileges.

> open `/etc/sudoers` with your editor of choice, altough I recommend using `visudo` just to be safe as it checks for errors before writing your changes
>
> append the following line to the file:
>
> `your-username ALL = NOPASSWD: /usr/bin/systemctl start xow.service , /usr/bin/systemctl stop xow.service , /usr/bin/systemctl restart xow.service , /usr/bin/systemctl daemon-reload`
>
> this should be obvious but replace the `your-username` part with your username




I know this project isn't much, but if you still wish to support me, even by just buying me a coffee, please consider supporting me via [PayPal](https://paypal.me/pools/c/8A6cgaZnse), or send me Monero to 4ACq44WNMHuiPn721mThrZKen24VYoERajQCmx8bCyB3XQiboJy4XUYQ688kHT26QzESyrcGng2dPQEJjPoT6x4pR9pS1Fq. Thank you 💓
